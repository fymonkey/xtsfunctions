#' Converts a PPY series to an index
#'
#' @export
#' @import xts
#' @importFrom zoo index
#'
#' @param x an xts
#' @param base the index number of the base of the PPY series
pp_to_index <- function(x, base = 100) {
    x <- xts::try.xts(x)
    
    ind <- zoo::index(x)
    
    if(!inherits(ind, "yearmon") && !inherits(ind, "yearqtr")) stop("Please provide monthly or quarterly xts")

    x <- chomp(x, sides = "left")
    
    base * cumprod(1 + x)
}
