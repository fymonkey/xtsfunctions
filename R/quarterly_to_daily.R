#' Function to approximate a quarterly series to a daily series.
#'
#' @export
#' @import xts
#' @importFrom zoo index na.approx
#'
#' @param x an xts
#' @param extrapolation the extrapolation function to apply to the series.  Default is na.approx.
#' @param ... additional arguments passed to extrapolation function.
quarterly_to_daily <- function(x, extrapolation = zoo::na.approx, ...) {
    x <- xts::try.xts(x)
    
    ind <- zoo::index(x)
    
    if(!inherits(ind, "yearqtr")) stop("Please provide quarterly xts")
    
    ind <- zoo::as.Date(ind + 1/4) - 1
    
    out_t <- seq(from = min(ind), to = max(ind), by = "days")
    out <- xts::xts(rep(NA, length(out_t)), order.by = out_t)
    
    out[ind] <- x
    
    out <- xts_rename(extrapolation(out, ...), names(x))
    xts::xtsAttributes(out) <- xts::xtsAttributes(x)
    out
}
