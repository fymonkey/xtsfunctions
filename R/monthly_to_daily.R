#' Function to approximate a monthly series to a daily series.
#'
#' @export
#' @import xts
#' @importFrom zoo index na.approx
#'
#' @param x an xts
#' @param extrapolation the extrapolation function to apply to the series.  Default is na.approx.
#' @param ... additional arguments passed to extrapolation function.
monthly_to_daily <- function(x, extrapolation = zoo::na.approx, ...) {
    x <- xts::try.xts(x)
    
    ind <- zoo::index(x)
    dat <- zoo::coredata(x)
    
    if(!inherits(ind, "yearmon")) stop("Please provide monthly xts")
    
    ind <- zoo::as.Date(ind + 1/12) - 1
    
    out_t <- seq(from = min(ind), to = max(ind), by = "days")
    
    mat <- matrix(nrow = length(out_t), ncol = ncol(dat))
    mat[out_t %in% ind,] <- dat
    
    out <- xts::xts(mat, order.by = out_t)
    
    out <- xts_rename(extrapolation(out, ...), names(x))
    xts::xtsAttributes(out) <- xts::xtsAttributes(x)
    out
}
