% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/xts_rename.R
\name{xts_rename}
\alias{xts_rename}
\title{Convenience function to rename an xts}
\usage{
xts_rename(x, name = NULL)
}
\arguments{
\item{x}{an xts}

\item{name}{name of the xts.  If left out the name of the xts will be the variable name passed in.}
}
\description{
Convenience function to rename an xts
}
